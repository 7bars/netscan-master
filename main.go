package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

const (
	ElasticSearchIP   = "127.0.0.1"
	ElasticSearchPORT = "9200"
	MongoIP           = "127.0.0.1"
	MongoPORT         = "32768"
)

func main() {
	// var result NmapRun
	// test3("nmap", &result)
	// search(nil,nil)
	// test4()
	server()
	/*
		var newSlave []IP

		readExistSlave(&newSlave)

		writeNewSlave(&NewSlave{"192.168.1.1"})
		// writeToMongo("nmap", &Person{"Nixi", "+34 77 2321 6543"})
		writeSlave(&Person{"Johny", "+99 55 2222 4444"})
		test2()

		var ip IP
		var lstIP listIP
		// ip.IP = "192.168.1.1-192.168.1.255"
		err := mainRequest("localhost", "8181", "/availableHost", &ip, &lstIP)
		if err != nil {
			fmt.Println(err)
		}
		if lstIP.IP[0] == "0.0.0.0" {
			//no hosts
		}
		// var scanResult NmapRun
		// err := scan("localhost", "8181", &scanResult)
		// if err != nil {
		// 	fmt.Println(err)
		// }

		client, err := elastic.NewClient()
		if err != nil {
			fmt.Println(err)
		}

		ctx := context.Background()
		termQuery := elastic.NewTermQuery("cve.affects.vendor.vendor_data.vendor_name", "mikrotik")
		searchResult, err := client.Search().
			Index("cve"). // search in index "twitter"
			Type("cve").
			Query(termQuery). // specify the query
			// Sort("user", true). // sort by "user" field, ascending
			// From(0).Size(10). // take documents 0-9
			Pretty(true). // pretty print request and response JSON
			Do(ctx)       // execute
		if err != nil {
			// Handle error
			panic(err)
		}

		// searchResult is of type SearchResult and returns hits, suggestions,
		// and all kinds of other information from Elasticsearch.
		fmt.Printf("Query took %d milliseconds\n", searchResult.TookInMillis)

		var ttyp CVE
		for _, item := range searchResult.Each(reflect.TypeOf(ttyp)) {
			t := item.(CVE)
			fmt.Println(t.LastModifiedDate)
			// t._source.Cve.Affects.Vendor.VendorData[0].VendorName
		}

		if searchResult.Hits.TotalHits > 0 {
			fmt.Printf("Found a total of %d tweets\n", searchResult.Hits.TotalHits)
		}
	*/
}

// func scan(ip string, port string, target interface{}) error {
// 	resp, err := http.Get("http://" + ip + ":" + port + "/scan?ip=localhost")
// 	if err != nil {
// 		return err
// 	}
// 	defer resp.Body.Close()
// 	return json.NewDecoder(resp.Body).Decode(target)
// }

func readScan() {
	dat, err := ioutil.ReadFile("1.json")
	if err != nil {
		fmt.Println(err)
	}
	var cve NmapRun
	err = json.Unmarshal(dat, &cve)
	if err != nil {
		fmt.Println(err)
	}
}

func test4() {
	var result NmapRun
	test3("nmap", &result)

	if len(result.Hosts) != 1 {
		fmt.Println("Result have to contain only 1 item!")
	} else {
		var vulnerability Vulnerability
		vulnerability.Service = make([]SVC, len(result.Hosts[0].Ports))
		for i := range result.Hosts[0].Ports {
			name := result.Hosts[0].Ports[i].Service.Name
			version := result.Hosts[0].Ports[i].Service.Version
			if version == "" {
				// vulnerability.Service[i].Problems = append(Problem,)
			} else {
				vulnerability.Service[i].Name = name
				vulnerability.Service[i].Version = version

				search(nil, &vulnerability.Service[i])
				
			}

		}
		ss := vulnerability.Service[1].Problems
		fmt.Println(ss)

	}
}
