package main

type CVE struct {
	Configurations struct {
		CVEDataVersion string `json:"CVE_data_version"`
		Nodes          []struct {
			Cpe      interface{} `json:"cpe"`
			Operator string      `json:"operator"`
		} `json:"nodes"`
	} `json:"configurations"`
	Cve struct {
		CVEDataMeta struct {
			ASSIGNER string `json:"ASSIGNER"`
			ID       string `json:"ID"`
		} `json:"CVE_data_meta"`
		Affects struct {
			Vendor struct {
				VendorData []struct {
					Product struct {
						ProductData []struct {
							ProductName string `json:"product_name"`
							Version     struct {
								VersionData []struct {
									VersionValue string `json:"version_value"`
								} `json:"version_data"`
							} `json:"version"`
						} `json:"product_data"`
					} `json:"product"`
					VendorName string `json:"vendor_name"`
				} `json:"vendor_data"`
			} `json:"vendor"`
		} `json:"affects"`
		DataFormat  string `json:"data_format"`
		DataType    string `json:"data_type"`
		DataVersion string `json:"data_version"`
		Description struct {
			DescriptionData []struct {
				Lang  string `json:"lang"`
				Value string `json:"value"`
			} `json:"description_data"`
		} `json:"description"`
		Problemtype struct {
			ProblemtypeData []struct {
				Description []struct {
					Lang  string `json:"lang"`
					Value string `json:"value"`
				} `json:"description"`
			} `json:"problemtype_data"`
		} `json:"problemtype"`
		References struct {
			ReferenceData []struct {
				Name      string   `json:"name"`
				Refsource string   `json:"refsource"`
				Tags      []string `json:"tags"`
				URL       string   `json:"url"`
			} `json:"reference_data"`
		} `json:"references"`
	} `json:"cve"`
	Impact struct {
		BaseMetricV2 struct {
			CvssV2 struct {
				AccessComplexity      string  `json:"accessComplexity"`
				AccessVector          string  `json:"accessVector"`
				Authentication        string  `json:"authentication"`
				AvailabilityImpact    string  `json:"availabilityImpact"`
				BaseScore             float64 `json:"baseScore"`
				ConfidentialityImpact string  `json:"confidentialityImpact"`
				IntegrityImpact       string  `json:"integrityImpact"`
				VectorString          string  `json:"vectorString"`
				Version               string  `json:"version"`
			} `json:"cvssV2"`
			ExploitabilityScore     float64 `json:"exploitabilityScore"`
			ImpactScore             float64 `json:"impactScore"`
			ObtainAllPrivilege      bool    `json:"obtainAllPrivilege"`
			ObtainOtherPrivilege    bool    `json:"obtainOtherPrivilege"`
			ObtainUserPrivilege     bool    `json:"obtainUserPrivilege"`
			Severity                string  `json:"severity"`
			UserInteractionRequired bool    `json:"userInteractionRequired"`
		} `json:"baseMetricV2"`
	} `json:"impact"`
	LastModifiedDate string `json:"lastModifiedDate"`
	PublishedDate    string `json:"publishedDate"`
}
