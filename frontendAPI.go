package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Slave struct {
	IP    string `json:"IP"`
	Port  string `json:"port"`
	State string `json:"state"`
	Host  struct {
		IP    []string `json:"IP"`
		State []string `json:"state"`
	} `json:"host"`
}

type Slave1 struct {
	IP   string `json:"IP"`
	Port string `json:"port"`
}

type State1 struct {
	State string `json:"state"`
}

type SlaveScan struct {
	IP     string `json:"IP"`
	Port   string `json:"port"`
	Target string `json:"target"`
}

// //Slave description
// type Slave struct {
// 	IP    string
// 	port  string
// 	state string
// 	host  SlaveHost
// }

// //SlaveHost description
// type SlaveHost struct {
// 	IP    []string
// 	state []string
// }

//Vulnerability description
// type Vulnerability struct {
// 	name        string
// 	description string
// 	versions    []string
// 	Severity    string
// 	links       []string
// }

//Vulnerability description
type Vulnerability struct {
	Service []SVC `json:"service"`
}

type SVC struct {
	Port     string    `json:"port"`
	Name     string    `json:"name"`
	Version  string    `json:"version"`
	Problems []Problem `json:"problems"`
}

type Problem struct {
	Description string `json:"description"`
	Score       string `json:"score"`
}

type Master struct {
	ElasticSearch string
	MongoDB       string
}

var slave []Slave

func add(w http.ResponseWriter, r *http.Request) {
	var (
		tmpSlave      Slave1
		tmpSlaveState State1
		tmpListIP     listIP
		err           error
		data          []byte
	)
	err = json.NewDecoder(r.Body).Decode(&tmpSlave)
	if err != nil {
		fmt.Println(err)
	}
	//check Is a slave alive
	err = getIpRequest(tmpSlave.IP, tmpSlave.Port, &tmpListIP)
	if err != nil {
		tmpSlaveState.State = "Error"
		// tmpSlave.State = "Error"
		data, err = json.Marshal(tmpSlaveState)
		if err != nil {
			fmt.Println(err)
		}
		w.Write(data)
	} else {
		//Slave is alive. Add to MongoDB
		go func(ip string, port string) {
			var newSlave []NewSlave
			tmp := false
			// var newSlave NewSlave
			readExistSlave(&newSlave)
			for i := range newSlave {
				if newSlave[i].IP == ip {
					tmp = true
					break
				}
			}
			if !tmp {
				writeNewSlave(&NewSlave{ip, port})
			}
		}(tmpSlave.IP, tmpSlave.Port)

		tmpSlaveState.State = "Ready"
		// tmpSlave.State = "Ready"
		// tmpSlave.Host.IP = tmpListIP.IP
		// slave = append(slave, tmpSlave)
		data, err = json.Marshal(tmpSlaveState)
		if err != nil {
			fmt.Println(err)
		}
		w.Write(data)
	}

}

func scan(w http.ResponseWriter, r *http.Request) {
	var (
		tmpSlave  SlaveScan
		tmpListIP listIP
		err       error
		// data   []byte
		result NmapRun
	)

	err = json.NewDecoder(r.Body).Decode(&tmpSlave)
	if err != nil {
		fmt.Println(err)
	}
	tmpListIP.IP = append(tmpListIP.IP, tmpSlave.Target)
	err = scanRequest(tmpSlave.IP, tmpSlave.Port, &tmpListIP, &result)
	if err != nil {
		fmt.Println(err)
		// data, err = json.Marshal(tmpSlave)
		// if err != nil {
		// 	fmt.Println(err)
		// }
		// w.Write(data)
	} else {
		//Create new thread, cuz we don't need to wait result it
		go writeNmap(&result)
		var vulnerability Vulnerability

		// test3("nmap", &result)

		if len(result.Hosts) != 1 {
			fmt.Println("Result have to contain only 1 item!")
			data, err := json.Marshal(vulnerability)
			if err != nil {
				fmt.Println(err)
			}
			w.Write(data)
		} else {

			vulnerability.Service = make([]SVC, len(result.Hosts[0].Ports))
			for i := range result.Hosts[0].Ports {
				vulnerability.Service[i].Name = result.Hosts[0].Ports[i].Service.Name
				vulnerability.Service[i].Version = result.Hosts[0].Ports[i].Service.Version
				if vulnerability.Service[i].Version == "" {
					// vulnerability.Service[i].Problems = append(Problem,)
				} else {
					// vulnerability.Service[i].Name = name
					// vulnerability.Service[i].Version = version

					search(nil, &vulnerability.Service[i])

				}

			}
			// ss := vulnerability.Service[1].Problems
			// fmt.Println(ss)
			data, err := json.Marshal(&vulnerability)
			if err != nil {
				fmt.Println(err)
			}
			w.Write(data)

		}
	}

}

func connect(w http.ResponseWriter, r *http.Request) {
	//take info about slave from mongo
	var newSlave []Slave1

	readExistSlave(&newSlave)
	// for i := range newSlave {
	// 	slave = append(slave, Slave{IP: newSlave[i].IP, State: "Wait..."})
	// 	// slave[i].IP = newSlave[i].IP
	// 	// slave[i].state = "Wait..."
	// }
	data, err := json.Marshal(newSlave)
	if err != nil {
		fmt.Println(err)
	}
	w.Write(data)
}

func alive(w http.ResponseWriter, r *http.Request) {
	//take info about client
	//check elastisearch and mongoDB
	//When I'll have a lot of time, I do it... it's mean never :)
	data, err := json.Marshal(&Master{"OK", "OK"})
	if err != nil {
		// fmt.Println("God will give you health")
		fmt.Println(err)
	}
	w.Write(data)
}

func hosts(w http.ResponseWriter, r *http.Request) {
	var tmpSlave Slave1
	var tmpListIP listIP
	var ip IP
	err := json.NewDecoder(r.Body).Decode(&tmpSlave)
	if err != nil {
		fmt.Println(err)
	}

	err = getIpRequest(tmpSlave.IP, tmpSlave.Port, &tmpListIP)
	if err != nil {
		//It is error
		tmpListIP.IP = append(tmpListIP.IP, "error")
		data, err := json.Marshal(tmpListIP)
		if err != nil {
			fmt.Println(err)
		}
		w.Write(data)
	} else {
		for i := range tmpListIP.IP {
			ip.IP += tmpListIP.IP[i]
			if len(tmpListIP.IP) != (i + 1) {
				ip.IP += ","
			}
		}
		availableHostRequest(tmpSlave.IP, tmpSlave.Port, &ip, &tmpListIP)
		data, err := json.Marshal(tmpListIP)
		if err != nil {
			fmt.Println(err)
		}
		w.Write(data)
	}

	// availableHostRequest(tmpSlave.IP, tmpSlave.Port, &ip)
}

func server() {
	//add() input Slave, return Slave(only change state). Add new slave to MongoDB
	http.HandleFunc("/add", add)
	http.HandleFunc("/scan", scan)
	http.HandleFunc("/connect", connect)
	http.HandleFunc("/hosts", hosts)
	http.HandleFunc("/alive", alive)

	http.ListenAndServe(":8282", nil)
}
