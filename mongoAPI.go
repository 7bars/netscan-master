package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/mongodb/mongo-go-driver/mongo/readpref"

	"github.com/mongodb/mongo-go-driver/mongo"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Person struct {
	Name  string
	Phone string
}

type NewSlave struct {
	IP   string
	Port string
}

//writeSlave save all data from slave
func writeNmap(target interface{}) {
	writeToMongo("nmap", target)
}

//writeSearch save result search
func writeSearch(target interface{}) {
	writeToMongo("result", target)
}

func writeNewSlave(target interface{}) {
	writeToMongo("slave", target)
}

func readExistSlave(output interface{}) {
	readFromMongo("slave", nil, output)
}

func readInfoResult(output interface{}) {

}

func readResult(curSlave string, output interface{}) {

}

// func writeToMongo(collection string, output interface{}) {
// 	session, err := mgo.Dial(MongoIP + ":" + MongoPORT)
// 	if err != nil {
// 		panic(err)
// 	}
// 	defer session.Close()

// 	// Optional. Switch the session to a monotonic behavior.
// 	session.SetMode(mgo.Monotonic, true)

// 	c := session.DB("report").C(collection)
// 	err = c.Insert(output)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// }

func readFromMongo(collection string, query bson.M, output interface{}) {
	session, err := mgo.Dial(MongoIP + ":" + MongoPORT)
	if err != nil {
		panic(err)
	}
	defer session.Close()
	// q := bson.M{query}
	c := session.DB("report").C(collection)
	err = c.Find(query).All(output)
	if err != nil {
		log.Fatal(err)
	}
}

func test2() {
	session, err := mgo.Dial("localhost:32768")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)

	c := session.DB("report").C("nmap")
	err = c.Insert(&Person{"Ale", "+55 53 8116 9639"},
		&Person{"Cla", "+55 53 8402 8510"})
	if err != nil {
		log.Fatal(err)
	}

	result := Person{}
	err = c.Find(bson.M{"name": "Ale"}).One(&result)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Phone:", result.Phone)
}

func writeToMongo(collectionName string, target interface{}) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, "mongodb://"+MongoIP+":"+MongoPORT)
	if err != nil {
		fmt.Println(err)
	}

	ctx, _ = context.WithTimeout(context.Background(), 2*time.Second)
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		fmt.Println(err)
	}

	collection := client.Database("report").Collection(collectionName)

	ctx, _ = context.WithTimeout(context.Background(), 5*time.Second)
	// _, err = collection.InsertOne(ctx, bson.M{"name": "pi", "value": 3.14159})
	_, err = collection.InsertOne(ctx, target)

	if err != nil {
		fmt.Println(err)
	}
}

func test3(collectionName string, target interface{}) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, "mongodb://"+MongoIP+":"+MongoPORT)
	if err != nil {
		fmt.Println(err)
	}

	ctx, _ = context.WithTimeout(context.Background(), 2*time.Second)
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		fmt.Println(err)
	}

	collection := client.Database("report").Collection(collectionName)

	filter := bson.M{"args": "nmap -sV -O -oX ./20181211115857.xml 192.168.56.66"}
	ctx, _ = context.WithTimeout(context.Background(), 5*time.Second)
	err = collection.FindOne(ctx, filter).Decode(target)
	if err != nil {
		fmt.Println(err)
	}
	// Do something with result...
}
