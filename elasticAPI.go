package main

import (
	"context"
	"fmt"
	"reflect"

	"github.com/olivere/elastic"
)

// func search(input interface{}, output interface{}) error {
// 	// query, err := json.Marshal(&input)
// 	// if err != nil {
// 	// 	return err
// 	// }

// 	url := "http://" + ElasticSearchIP + ":" + ElasticSearchPORT + "/cve/cve/_search"

// 	req, err := http.NewRequest("GET", url, bytes.NewBuffer([]byte(`{"query":{"simple_query_string":{"query":"mikrotik","fields":[]}}}`)))
// 	if err != nil {
// 		return err
// 	}
// 	req.Header.Set("Content-Type", "application/json")

// 	client := &http.Client{}
// 	resp, err := client.Do(req)
// 	if err != nil {
// 		return err
// 	}
// 	defer resp.Body.Close()
// 	fmt.Println(ioutil.ReadAll(resp.Body))
// 	return json.NewDecoder(resp.Body).Decode(output)
// }

func search(target *NmapRun, output *SVC) {
	client, err := elastic.NewClient()
	if err != nil {
		fmt.Println(err)
	}

	ctx := context.Background()
	// termQuery := elastic.NewTermQuery("cve.affects.vendor.vendor_data.vendor_name", "mysql")
	qq := elastic.NewBoolQuery()
	qq = qq.Must(elastic.NewTermQuery("cve.affects.vendor.vendor_data.product.product_data.product_name", output.Name))
	qq = qq.Filter(elastic.NewTermQuery("cve.affects.vendor.vendor_data.product.product_data.version.version_data.version_value", output.Version))

	searchResult, err := client.Search().
		Index("cve"). // search in index "twitter"
		Type("cve").
		// Query(termQuery). // specify the query
		Query(qq).
		// Sort("user", true). // sort by "user" field, ascending
		// Sort("cve.affects.vendor.vendor_data.vendor_name", "5.5.0"). // sort by "user" field, ascending
		// From(0).Size(10). // take documents 0-9
		Pretty(true). // pretty print request and response JSON
		Do(ctx)       // execute
	if err != nil {
		// Handle error
		panic(err)
	}

	// searchResult is of type SearchResult and returns hits, suggestions,
	// and all kinds of other information from Elasticsearch.
	fmt.Printf("Query took %d milliseconds\nFound items: %d", searchResult.TookInMillis, searchResult.Hits.TotalHits)

	var ttyp CVE
	output.Problems = make([]Problem, searchResult.Hits.TotalHits)
	for i, item := range searchResult.Each(reflect.TypeOf(ttyp)) {
		t := item.(CVE)
		fmt.Println(t.LastModifiedDate)
		fmt.Println(t.Cve.Description.DescriptionData[0].Value)
		// t._source.Cve.Affects.Vendor.VendorData[0].VendorName

		output.Problems[i].Description = t.Cve.Description.DescriptionData[0].Value
		output.Problems[i].Score = fmt.Sprint(t.Impact.BaseMetricV2.CvssV2.BaseScore)
	}

	if searchResult.Hits.TotalHits > 0 {
		fmt.Printf("Found a total of %d tweets\n", searchResult.Hits.TotalHits)
	}
}
