package main

import (
	"bytes"
	"encoding/json"
	"net/http"
)

type listIP struct {
	IP []string
}

type IP struct {
	IP string
}

func getIpRequest(ip string, port string, target interface{}) error {

	return mainRequest(ip, port, "/getIP", nil, target)
}

func availableHostRequest(ip string, port string, parameters interface{}, target interface{}) {
	mainRequest(ip, port, "/availableHost", parameters, target)
}

func scanRequest(ip string, port string, parameters interface{}, target interface{}) error {
	return mainRequest(ip, port, "/scan", parameters, target)
}

func mainRequest(ip string, port string, call string, parameters interface{}, target interface{}) error {

	// if parameters != nil {
	param, err := json.Marshal(parameters)
	if err != nil {
		return err
	}
	// } else {
	// 	var param []byte
	// }

	url := "http://" + ip + ":" + port + call
	req, err := http.NewRequest("GET", url, bytes.NewBuffer(param))
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return json.NewDecoder(resp.Body).Decode(target)
}
